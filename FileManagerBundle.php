<?php

declare(strict_types=1);

namespace HakimCh\FileManagerBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class FileManagerBundle extends Bundle
{
}
