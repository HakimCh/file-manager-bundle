<?php

declare(strict_types=1);

namespace HakimCh\FileManagerBundle\Command;

use App\Entity\Post;
use Doctrine\ORM\EntityManagerInterface;
use HakimCh\FileManagerBundle\Entity\FileManager;
use HakimCh\FileManagerBundle\Services\Upload;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\File\File;

class MigrateCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @var string
     */
    private $imagePath;

    public function __construct(EntityManagerInterface $manager, string $imagePath, ?string $name = null)
    {
        parent::__construct($name);
        $this->manager = $manager;
        $this->imagePath = $imagePath;
    }

    protected function configure(): void
    {
        parent::configure();

        $this
            ->setName('hakimch:filemanager:migrate')
            ->setDescription('Migrate the legacy')
            ->setHelp('This command allows you to migrate legacy files to the new one...');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $output->writeln('');
        $repo = $this->manager->getRepository(Post::class);
        $upload = new Upload($this->imagePath.'filemanager/');
        $old = $new = [];
        $posts = $repo->findAllByCriteria([]);

        $progressBar = new ProgressBar($output, \count($posts));
        $progressBar->start();

        foreach ($posts as $post) {
            $id = $post->getId();
            $content = $post->getContent();
            preg_match_all('@src="https://www.sportautomoto.ma/images/([^"]+)"@', $content, $maches);
            if (isset($maches[1]) && !empty($maches[1])) {
                foreach ($maches[1] as $mach) {
                    if ($filename = $this->process($mach, $upload)) {
                        $old[$id][] = 'https://www.sportautomoto.ma/images/'.$mach;
                        $new[$id][] = '/images/filemanager/'.$filename;
                        $fileManager = new FileManager();
                        $fileManager->setFilename($filename);
                        $fileManager->setCreatedAt($post->getCreatedAt());
                        $this->manager->persist($fileManager);
                        $this->manager->flush();
                    }
                }
            }

            if (isset($old[$id])) {
                try {
                    $newContent = str_replace($old[$id], $new[$id], $content);
                    $post->setContent($newContent);
                    $this->manager->persist($post);
                    $this->manager->flush();
                } catch (\Exception $e) {
                    dump($old[$id], $new[$id]);
                    die;
                }
            }

            $progressBar->advance();
        }

        $progressBar->finish();
    }

    private function process($filename, Upload $upload)
    {
        $fullPath = str_replace(['\\', '/'], \DIRECTORY_SEPARATOR, $this->imagePath.$filename);
        try {
            $file = new File($fullPath);

            return $upload->update($file);
        } catch (\Exception $e) {
            return null;
        }
    }
}
