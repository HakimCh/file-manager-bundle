<?php

declare(strict_types=1);

namespace HakimCh\FileManagerBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use HakimCh\FileManagerBundle\Entity\FileManager;
use HakimCh\FileManagerBundle\Services\PhpArrayCache;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RefreshIndexCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var PhpArrayCache
     */
    private $cache;

    public function __construct(EntityManagerInterface $manager, PhpArrayCache $cache, ?string $name = null)
    {
        parent::__construct($name);
        $this->manager = $manager;
        $this->cache = $cache;
    }

    protected function configure(): void
    {
        parent::configure();

        $this
            ->setName('hakimch:filemanager:index')
            ->setDescription('Refresh the index.')
            ->setHelp('This command allows you to refresh the index...');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $output->writeln('');

        $items = $this->manager->getRepository(FileManager::class)->findAll();
        $index = [];

        $progressBar = new ProgressBar($output, \count($items));
        $progressBar->start();

        foreach ($items as $item) {
            $year = $item->getCreatedAt()->format('Y');
            $month = $item->getCreatedAt()->format('m');
            $index[$year.$month][] = '/images/filemanager/'.$item->getFilename();
            $progressBar->advance();
        }

        $progressBar = new ProgressBar($output, \count($items));
        $progressBar->start();

        foreach ($index as $key => $value) {
            $this->cache->set($key, $value);
        }

        $progressBar->finish();
    }
}
