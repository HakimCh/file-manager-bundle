<?php

declare(strict_types=1);

namespace HakimCh\FileManagerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="HakimCh\FileManagerBundle\Repository\FileManagerRepository")
 */
class FileManager
{
    public const MAX_RESULT_BY_PAGE = 40;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\Image
     */
    private $filename;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default" = 0}))
     */
    private $isRemoved;

    /**
     * @ORM\Column(type="datetime", nullable=true, options={"default" = "CURRENT_TIMESTAMP"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true, options={"default" = "1970-01-01"})
     */
    private $removedAt;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getFilename()
    {
        return $this->filename;
    }

    public function setFilename($filename): void
    {
        $this->filename = $filename;
    }

    public function getisRemoved()
    {
        return $this->isRemoved;
    }

    public function setIsRemoved($isRemoved): void
    {
        $this->isRemoved = $isRemoved;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getRemovedAt()
    {
        return $this->removedAt;
    }

    public function setRemovedAt($removedAt): void
    {
        $this->removedAt = $removedAt;
    }
}
