<?php

declare(strict_types=1);

namespace HakimCh\FileManagerBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use HakimCh\FileManagerBundle\Entity\FileManager;
use Symfony\Bridge\Doctrine\RegistryInterface;

class FileManagerRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, FileManager::class);
    }

    public function getFolders($maxResults = 10)
    {
        return $this->createQueryBuilder('f')
            ->select("DATE_FORMAT(f.createdAt, '%Y') as year", "DATE_FORMAT(f.createdAt, '%m') as digi_month", "DATE_FORMAT(f.createdAt, '%M') as month")
            ->setMaxResults($maxResults)
            ->groupBy('year', 'month', 'digi_month')
            ->addOrderBy('year', 'DESC')
            ->addOrderBy('digi_month', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function findByYearMonth($year = null, $month = null, $page = 1, $maxResults = FileManager::MAX_RESULT_BY_PAGE)
    {
        if (!$year) {
            $year = date('Y');
            $month = date('m');
        }

        $firstResult = ($page - 1) * $maxResults;

        return $this->createQueryBuilder('f')
            ->andWhere("DATE_FORMAT(f.createdAt, '%m%Y') = :createdAt")
            ->setParameter('createdAt', sprintf('%s%s', $month, $year))
            ->setFirstResult($firstResult)
            ->setMaxResults($maxResults)
            ->orderBy('f.createdAt', 'DESC')
            ->getQuery()
            ->getArrayResult();
    }

    public function countByYearMonth($year = null, $month = null)
    {
        if (!$year) {
            $year = date('Y');
            $month = date('m');
        }

        return $this->createQueryBuilder('f')
            ->select('COUNT(f.id) as count')
            ->andWhere("DATE_FORMAT(f.createdAt, '%m%Y') = :createdAt")
            ->setParameter('createdAt', sprintf('%s%s', $month, $year))
            ->getQuery()
            ->getOneOrNullResult();
    }
}
