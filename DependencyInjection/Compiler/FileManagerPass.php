<?php

declare(strict_types=1);

namespace HakimCh\FileManagerBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class FileManagerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $def = $container->getDefinition('twig');
        $def->addMethodCall('addGlobal', [
            'file_manager' => [
                'images_path' => $this->getAssetFolderPath($container->getParameter('file_manager.images_path')),
                'thumbs_path' => $this->getAssetFolderPath($container->getParameter('file_manager.thumbs_path')),
            ],
        ]);
    }

    private function getAssetFolderPath($path)
    {
        return strstr($path, 'images/');
    }
}
