<?php

declare(strict_types=1);

namespace HakimCh\FileManagerBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('file_manager');

        $treeBuilder
            ->getRootNode()
            ->children()
                ->scalarNode('images_path')->end()
                ->scalarNode('thumbs_path')->end()
            ->end();

        return $treeBuilder;
    }
}
