<?php

declare(strict_types=1);

namespace HakimCh\FileManagerBundle\Services;

use Doctrine\ORM\EntityManagerInterface;
use HakimCh\FileManagerBundle\Entity\FileManager;
use Symfony\Component\Console\Output\OutputInterface;

class FileIndexer
{
    /**
     * @var PhpArrayCache
     */
    private $cache;

    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @var string
     */
    private $imagePath;

    public function __construct(string $imagePath, EntityManagerInterface $manager, PhpArrayCache $cache)
    {
        $this->cache = $cache;
        $this->imagePath = $imagePath;
        $this->manager = $manager;
    }

    public function refresh($key, OutputInterface $output)
    {
        $data = [];
        $files = $this->manager->getRepository(FileManager::class)->findAll();

        foreach ($files as $file) {
            $filePath = $this->imagePath.'filemanager/'.$file->getFilename();
            if (is_file($filePath)) {
                $data[] = strstr($item, 'images/');
            }
        }

        $this->cache->set($key, $data);

        return \count($data);
    }
}
