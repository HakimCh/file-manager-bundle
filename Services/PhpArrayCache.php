<?php

declare(strict_types=1);

namespace HakimCh\FileManagerBundle\Services;

class PhpArrayCache
{
    /**
     * @var string
     */
    private $cacheDir;

    private $extension;

    public function __construct(string $cacheDir, $extension = '.php')
    {
        $this->checkDir($cacheDir);
        $this->extension = $extension;
    }

    public function get($key, $default = null)
    {
        if ($this->has($key)) {
            return require $this->getFileName($key);
        }

        return [];
    }

    public function set($key, $value, $ttl = null)
    {
        file_put_contents(
            $this->getFileName($key),
            "<?php\n\nreturn ".var_export($value, true).";\n"
        );
    }

    public function delete($key)
    {
        if ($this->has($key)) {
            unlink($this->getFileName($key));
        }
    }

    public function invalidate($prefix = null)
    {
        foreach (glob($this->cacheDir.$prefix.'*') as $item) {
            unlink($item);
        }
    }

    public function refresh($prefix = null)
    {
        foreach (glob($this->cacheDir.$prefix.'*') as $item) {
            unlink($item);
        }
    }

    public function has($key)
    {
        return file_exists($this->getFileName($key));
    }

    private function getFileName($key)
    {
        return $this->cacheDir.$key.$this->extension;
    }

    private function checkDir($cacheDir)
    {
        $this->cacheDir = $cacheDir;

        if (!is_dir($cacheDir)) {
            mkdir($cacheDir, 0777, true);
        }
    }
}
