<?php

declare(strict_types=1);

namespace HakimCh\FileManagerBundle\Services;

use HakimCh\FileManagerBundle\Exceptions\UploadDimensionException;
use Imagine\Gd\Imagine;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Upload
{
    /**
     * @var string
     */
    private $path;

    public function __construct(string $path)
    {
        $this->path = $path;
    }

    public function update(File $file): string
    {
        $fileName = $this->generateUniqueFileName($file);

        [$width, $heigth] = getimagesize($file->getPathname());

        if ($width > UploadDimensionException::IMG_MAX_WIDTH) {
            throw new UploadDimensionException($width);
        }

        $imagine = (new Imagine())->open($file->getPathname());

        $widen = $this->getWiden($width, $heigth);
        $image = $imagine->resize($imagine->getSize()->widen($widen));

        if ('png' == $file->getExtension()) {
            $image->save($this->path.$fileName, ['png_compression_level' => 9]);
        } elseif (\in_array($file->getExtension(), ['jpeg', 'jpg'])) {
            $image->save($this->path.$fileName, ['jpeg_quality' => 90]);
        } else {
            $image->save($this->path.$fileName);
        }

        return $fileName;
    }

    public function move(File $file): string
    {
        $fileName = $this->generateUniqueFileName($file);

        [$width, $heigth] = getimagesize($file->getPathname());

        if ($width > UploadDimensionException::IMG_MAX_WIDTH) {
            throw new UploadDimensionException($width);
        }

        $file->move($this->path, $fileName);

        return $fileName;
    }

    public function generateUniqueFileName(File $file): string
    {
        $originalName = $file instanceof UploadedFile ? $file->getClientOriginalName() : $file->getFilename();

        $explodedFilename = explode('.', $originalName);
        $fileExtension = '.'.end($explodedFilename);
        $rawFilename = str_replace($fileExtension, '', $originalName);
        $rawFilename = preg_replace('/[^a-zA-Z0-9\-\._]/', '', $rawFilename);
        $filename = str_replace(['.', '_'], '-', $rawFilename);

        return strtolower($filename.$fileExtension);
    }

    private function getWiden($w, $h)
    {
        $p = 1;
        if ($h > $w) {
            if ($h > 1280) {
                $p = (1280 * 100 / $h) / 100;
            }
        } elseif ($w > 1920) {
            $p = (1920 * 100 / $w) / 100;
        }

        return $w * $p;
    }
}
