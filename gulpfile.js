var gulpfile = require('gulp'),
    less = require('gulp-less'),
    concat = require('gulp-concat'),
    cssmin = require('gulp-cssmin');

gulpfile.task('style', function () {
    return gulpfile.src("./assets/less/style.less")
        .pipe(less().on('error', function(err) { console.log(err) }))
        .pipe(cssmin().on('error', function(err) { console.log(err) }))
        .pipe(gulpfile.dest('./Resources/public/css/'));
});

gulpfile.task('watch', function () {
    gulpfile.watch('./assets/less/*.less', ['style']);
});

gulpfile.task('default', ['style', 'watch']);