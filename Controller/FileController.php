<?php

declare(strict_types=1);

namespace HakimCh\FileManagerBundle\Controller;

use HakimCh\FileManagerBundle\Entity\FileManager;
use HakimCh\FileManagerBundle\Exceptions\UploadDimensionException;
use HakimCh\FileManagerBundle\Form\UploadType;
use HakimCh\FileManagerBundle\Services\PhpArrayCache;
use HakimCh\FileManagerBundle\Services\Upload;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/filemanager/api", name="file_manager_api_")
 */
class FileController extends AbstractController
{
    /**
     * @var PhpArrayCache
     */
    private $cache;

    public function __construct(PhpArrayCache $cache)
    {
        $this->cache = $cache;
    }

    /**
     * @Route("/file/{id}", methods={"DELETE"}, name="image_remove")
     *
     * @param FileManager $file
     *
     * @return Response
     */
    public function remove(FileManager $file): Response
    {
        try {
            $manager = $this->getDoctrine()->getManager();
            $manager->remove($file);
            $manager->flush();

            if (is_file($filePath = $this->getParameter('file_manager.images_path').'filemanager/'.$file->getFilename())) {
                unlink($filePath);
            }
            $data = [
                'success' => true,
                'data' => $file->getFilename().' removed with success',
            ];
        } catch (\Exception $e) {
            $data = [
                'success' => false,
                'data' => $e->getMessage(),
            ];
        }

        return $this->json($data);
    }

    /**
     * @Route("/file/{page}", defaults={"page" = 1}, methods={"POST"}, name="image_add")
     *
     * @param Request $request
     * @param mixed   $page
     *
     * @return Response
     */
    public function add(Request $request, $page): Response
    {
        $data = ['success' => false];

        $fileManager = new FileManager();
        $form = $this->createForm(UploadType::class, $fileManager);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $fileManager->getFilename();

            try {
                $upload = new Upload($this->getParameter('file_manager.images_path').'filemanager/');
                $fileName = $upload->update($file);

                $fileManager->setFilename($fileName);

                $em = $this->getDoctrine()->getManager();
                $em->persist($fileManager);
                $em->flush();

                $data = ['success' => true, 'data' => $fileName];
            } catch (UploadDimensionException $e) {
                $data['data'] = $e->getMessage();
            }
        }

        if ($data['success']) {
            $this->refreshCache($page);
            $form->initialize();
        }

        return $this->json($data);
    }

    /**
     * @Route("/{year}/{month}/{page}", defaults={"page" = 1}, methods={"GET"}, name="by_year_month")
     *
     * @param mixed $year
     * @param mixed $month
     * @param mixed $page
     *
     * @return Response
     */
    public function read($year, $month, $page): Response
    {
        $prev = $next = '#end';
        $maxResults = $pagesCount = 0;
        $cacheId = sprintf('%s%s%s', $year, $month, $page);

        if ($this->cache->has($cacheId)) {
            $files = $this->readFromCache($cacheId, $maxResults, $pagesCount);
        } else {
            $files = $this->readFromDb($year, $month, $page, $maxResults, $pagesCount);
        }

        if ($page > 1) {
            $prev = $this->get('router')->generate('file_manager_api_by_year_month', ['year' => $year, 'month' => $month, 'page' => $page - 1]);
        }
        if ($page < $pagesCount) {
            $next = $this->get('router')->generate('file_manager_api_by_year_month', ['year' => $year, 'month' => $month, 'page' => $page + 1]);
        }

        $response = new Response();
        $response->setSharedMaxAge(3600);
        $response = $this->render('@FileManager/partials/index.html.twig', [
            'files' => $files,
            'prev' => $prev,
            'next' => $next,
            'page' => $page,
        ], $response);

        return $response;
    }

    private function readFromCache($cacheId, &$maxResults, &$pagesCount)
    {
        $cache = $this->cache->get($cacheId);
        $maxResults = $cache['maxResults'];
        $pagesCount = $cache['pagesCount'];

        return $cache['files'];
    }

    private function readFromDb($year, $month, $page, &$maxResults, &$pagesCount)
    {
        $repo = $this->getDoctrine()->getRepository(FileManager::class);

        if ($result = $repo->countByYearMonth($year, $month)) {
            $maxResults = $result['count'];
        }

        $pagesCount = (int) round($maxResults / FileManager::MAX_RESULT_BY_PAGE, 0);

        $files = $repo->findByYearMonth($year, $month, $page);

        $this->cache->set(
            sprintf('%s%s%s', $year, $month, $page),
            [
                'pagesCount' => $pagesCount,
                'maxResults' => $maxResults,
                'files' => $files,
            ]
        );

        return $files;
    }

    private function refreshCache($page)
    {
        $maxResults = $pagesCount = 0;
        $this->readFromDb(date('Y'), date('m'), $page, $maxResults, $pagesCount);
    }
}
