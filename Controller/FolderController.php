<?php

declare(strict_types=1);

namespace HakimCh\FileManagerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/filemanager/api/folder", name="file_manager_api_folder_")
 */
class FolderController extends AbstractController
{
    /**
     * @Route("/", methods={"POST"}, name="add")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function add(Request $request): Response
    {
        $data = [
            'success' => true,
            'data' => '',
        ];

        return $this->json($data);
    }
}
