<?php

declare(strict_types=1);

namespace HakimCh\FileManagerBundle\Controller;

use HakimCh\FileManagerBundle\Entity\FileManager;
use HakimCh\FileManagerBundle\Form\UploadType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/filemanager", name="file_manager_home")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function home(Request $request): Response
    {
        $repo = $this->getDoctrine()->getRepository(FileManager::class);

        return $this->render('@FileManager/home/index.html.twig', [
            'form' => $this->createForm(UploadType::class)->createView(),
            'files' => $repo->findByYearMonth(),
            'folders' => $repo->getFolders(),
        ]);
    }
}
