<?php

declare(strict_types=1);

namespace HakimCh\FileManagerBundle\Exceptions;

use Throwable;

class UploadDimensionException extends \Exception
{
    public const IMG_MAX_WIDTH = 7680;

    public function __construct(int $width, int $code = 0, Throwable $previous = null)
    {
        parent::__construct($this->formatMessage($width), $code, $previous);
    }

    public function formatMessage($width)
    {
        return sprintf(
            'The image width (%spx) excess the allowed limit (8k = %spx)',
            number_format($width, 0, '.', ' '),
            self::IMG_MAX_WIDTH
        );
    }
}
