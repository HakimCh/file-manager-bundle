function tinyMceFileManagerCallback(field_name, url, type, win, userParams = {})
{
    const params = Object.assign({
        file: "/filemanager",
        title : 'Gestionnaire de média',
        width : window.innerWidth * 0.75,
        height : window.innerHeight * 0.8,
        resizable : "no",
        inline : "yes",
        close_previous : "no"
    }, userParams);

    tinyMCE.activeEditor.windowManager.open(params, {
        window : win,
        input : field_name
    });

    return false;
}
