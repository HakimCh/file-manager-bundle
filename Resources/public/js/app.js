(($) => {

    const content = $("#content");
    const height = $(window).height() - 160;

    content.css({
        'max-height': height,
        'height': height,
        'overflow': 'auto'
    });

    $('.remove-image').on('click', function(e) {
        $.ajax({
            url: $(this).attr('href'),
            type: 'DELETE',
            dataType: 'json',
            success: function (response) {
                navigate(base_url);
                console.log(response);
            }
        });

        e.preventDefault();
    });

    $('#addFile input[type="file"]').change(function(e) {
        const form = $(this).parent();
        const formdata = (window.FormData) ? new FormData(form[0]) : null;
        const data     = (formdata !== null) ? formdata : form.serialize();

        $.ajax({
            url: form.attr('action') + '/' + $('#addFile').data('page'),
            type: 'POST',
            contentType: false,
            processData: false,
            dataType: 'json',
            async: true,
            data: data,
            success: function (response) {
                navigate(base_url);
                console.log(response);
            },
            always: function () {
                form[0].reset();
            }
        });
        
        e.preventDefault();
    });

    $('.image', content).click(function(e) {
        tinymceInsert($(this).data('image'));
    });

    $('.show-by-month').click(function(e) {
        const href = $(this).attr('href');
        if (href != '#end') {
            navigate(href);
        }
        e.preventDefault();
    });

    let navigate = (href) => {
        $.get(href).done(function(data) {
            content.html(data);
            const body = $(data);
            $('#addFile').attr('data-page', body.data('page'));
            $('#btn-prev').attr('href', body.data('prev'));
            $('#btn-next').attr('href', body.data('next'));
        });
    };

    let request = (ele, data, method, success) => {
        $.ajax({
            data: data,
            method: method,
            dataType: 'json',
            url: ele.attr('href'),
            success: success
        });
    };

    let tinymceInsert = (image_link) => {
        if (typeof tinymceWindow !== 'undefined') {
            tinymceWindow.document.getElementById(tinymceInput).value = image_link;
            tinyMceWindowManager.close();
        } else {
            console.log('TinyMCE not loaded!');
        }
    };

})(jQuery)